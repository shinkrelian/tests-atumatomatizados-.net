﻿Imports CalculadoraUI
Imports Interfaces
Imports Moq
Imports NUnit.Framework

Public Class Persona
    Public Overridable Property Nombre() As String
        Get
            Return "Nombre"
        End Get
        Set
        End Set
    End Property
End Class


<TestFixture>
Public Class MocksTests

    Private _mock As Mock(Of ICalculadora)
    Private _eventArgs As EventArgs
    Private _sender As Object
    Private _calcMock As ICalculadora
    Private _calcForm As CalcForm

    <SetUp()>
    Public Sub SetUp()
        _mock = New Mock(Of ICalculadora)
        _eventArgs = (New Mock(Of EventArgs)).Object
        _sender = (New Mock(Of Object)).Object
        _calcMock = _mock.Object
        _calcForm = New CalcForm(_calcMock)
    End Sub

    <Test>
    Public Sub ValorDeRetorno()

        'Fijo
        _mock.Setup(
            Function(c) c.Sumar(It.IsAny(Of Integer), It.IsAny(Of Integer))) _
            .Returns(4)
        Assert.That(_calcMock.Sumar(2, 2), [Is].EqualTo(4))
        Assert.That(_calcMock.Sumar(3, 5), [Is].EqualTo(4))

        'Función
        _mock.Setup(
            Function(c) c.Sumar(It.IsAny(Of Integer), It.IsAny(Of Integer))) _
            .Returns(Function(a, b) a + b + 1)
        Assert.That(_calcMock.Sumar(2, 2), [Is].EqualTo(5))
        Assert.That(_calcMock.Sumar(3, 5), [Is].EqualTo(9))

    End Sub

    <Test>
    Public Sub Parametros()

        'Sólo responder a parámetros específicos
        _mock.Setup(Function(c) c.Sumar(It.IsIn({4, 2, 2}), 2)).Returns(10)
        _mock.Setup(Function(c) c.Sumar(3, 8)).Returns(0)

        Assert.That(_calcMock.Sumar(4, 2), [Is].EqualTo(10))
        Assert.That(_calcMock.Sumar(3, 5), [Is].Not.EqualTo(10))
        Assert.That(_calcMock.Sumar(3, 8), [Is].EqualTo(0))

    End Sub

    <Test>
    Public Sub Excepciones()

        'Lanzar una exception
        _mock.Setup(Function(c) c.Sumar(4, 2)).
            Throws(Of ApplicationException)()

        _calcMock.Sumar(1, 1)
        _calcMock.Sumar(2, 3)
        Assert.That(Sub() _calcMock.Sumar(4, 2),
            Throws.TypeOf(Of ApplicationException))

    End Sub

    <Test>
    Public Sub Propiedades()

        Dim personaMock = New Mock(Of Persona)
        personaMock.SetupGet(Function(p) p.Nombre).Returns("Javier")
        Assert.That(personaMock.Object.Nombre, [Is].EqualTo("Javier"))

    End Sub

    <Test>
    Public Sub Validaciones()

        _calcMock.Sumar(4, 2)

        _mock.Verify(Sub(c) c.Sumar(4, 2))
        _mock.Verify(Sub(c) c.Sumar(4, 2), Times.Once())
        _mock.Verify(Sub(c) c.Sumar(5, 1), Times.Exactly(0))

    End Sub

End Class
