﻿Imports BowlingGame
Imports NUnit
Imports NUnit.Framework

<TestFixture>
Public Class GameTests

    Private game As Game

    <SetUp()>
    Public Sub SetUp()
        game = New Game
    End Sub

    <Test()>
    Public Sub PuedeEjecutarRoll()
        game.Roll(0)
    End Sub

    <Test()>
    Public Sub PuedeEjecutarScore()
        Dim score As Integer = game.Score()
    End Sub

    <Test()>
    Public Sub FalloTodos()
        HacerVariosRolls(20, 0)
        Assert.That(game.Score(), [Is].EqualTo(0))
    End Sub

    <Test()>
    Public Sub TiroUnPino()
        game.Roll(1)
        HacerVariosRolls(19, 0)
        Assert.That(game.Score(), [Is].EqualTo(1))
    End Sub

    <Test()>
    Public Sub TiroDosPinos()
        game.Roll(1)
        game.Roll(2)
        HacerVariosRolls(18, 0)
        Assert.That(game.Score(), [Is].EqualTo(3))
    End Sub

    <Test()>
    Public Sub TiroPurosUnos()
        HacerVariosRolls(20, 1)
        Assert.That(game.Score(), [Is].EqualTo(20))
    End Sub

    <Test()>
    Public Sub UnSpare()
        LanzarSpare()
        game.Roll(3)
        HacerVariosRolls(17, 0)
        Assert.That(game.Score(), [Is].EqualTo(16))
    End Sub

    <Test()>
    Public Sub DosSpare()
        LanzarSpare()
        game.Roll(3)
        game.Roll(1)
        LanzarSpare()
        game.Roll(3)
        HacerVariosRolls(13, 0)
        Assert.That(game.Score(), [Is].EqualTo(33))
    End Sub

    <Test()>
    Public Sub UnaChuza()
        LanzarChuza()
        game.Roll(2)
        game.Roll(1)

        HacerVariosRolls(16, 0)
        Assert.That(game.Score(), [Is].EqualTo(16))
    End Sub

    <Test()>
    Public Sub JuegoPerfecto()
        For i As Integer = 1 To 12
            LanzarChuza()
        Next
        Assert.That(game.Score(), 300)
    End Sub

    Private Sub LanzarChuza()
        game.Roll(10)
    End Sub

    Private Sub LanzarSpare()
        game.Roll(2)
        game.Roll(8)
    End Sub

    Private Sub HacerVariosRolls(numero As Integer, pins As Integer)
        For i As Integer = 1 To numero
            game.Roll(pins)
        Next
    End Sub

End Class
