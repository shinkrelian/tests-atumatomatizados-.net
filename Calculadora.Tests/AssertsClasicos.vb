﻿Imports NUnit.Framework

<TestFixture>
Public Class AssertsClasicos

    <Test()>
    Public Sub AssertsClasicosTest()

        Assert.True(True)
        Assert.False(False)
        Assert.AreEqual(2, 2)
        Assert.AreNotEqual(2, 3)

        Assert.Greater(4, 3)
        Assert.GreaterOrEqual(4, 3)
        Assert.Less(3, 4)
        Assert.LessOrEqual(3, 4)

        Dim anObject As Object
        Assert.Null(anObject)

        anObject = New Object
        Assert.NotNull(anObject)

    End Sub

End Class
