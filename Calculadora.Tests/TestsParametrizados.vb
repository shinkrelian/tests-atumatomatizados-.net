﻿Imports NUnit.Framework

<TestFixture>
Public Class TestsParametrizados

    <Test(Description:="Random permite generar valores aliatorios para el test. Recibe el minimo, maximo y numero de valores requeridos")>
    Public Sub SumaRandom(
            <Random(1, 100, 3)> a As Integer,
            <Random(100, 200, 3)> b As Integer)
        Dim calculadora As Calculadora = New Calculadora()
        Assert.That(a + b, calculadora.Sumar(a, b))
    End Sub

    <Test(Description:="Values permite gerenar valores específicos para el test")>
    Public Sub SumaValues(
            <Values(1, 2, 3)> a As Integer,
            <Values(3, 4, 5)> b As Integer)
        Dim calculadora As Calculadora = New Calculadora()
        Assert.That(a + b, calculadora.Sumar(a, b))
    End Sub

    <Test(Description:="Range permite gerenar valores dentro de un rango de valores. Recibe el minimo, maximo y salto entre valores")>
    Public Sub SumaRange(
            <Range(1, 6, 2)> a As Integer,
            <Range(3, 9, 3)> b As Integer)
        Dim calculadora As Calculadora = New Calculadora()
        Assert.That(a + b, calculadora.Sumar(a, b))
    End Sub

    <Test(Description:="Sequential combina los valores secuencialemnte. En el ejemplo: ((1,3),(2,4),(3,5)) ")>
    <Sequential()>
    Public Sub SumaSequetial(
            <Values(1, 2, 3)> a As Integer,
            <Values(3, 4, 5)> b As Integer)
        Dim calculadora As Calculadora = New Calculadora()
        Assert.That(a + b, calculadora.Sumar(a, b))
    End Sub

    <Test(Description:="Combinatorial genera todas las combinaciones. En el ejemplo: ((1,3),(1,4),(2,3),(2,4)) ")>
    <Combinatorial()>
    Public Sub SumaCombinatorial(
            <Values(1, 2)> a As Integer,
            <Values(3, 4)> b As Integer)
        Dim calculadora As Calculadora = New Calculadora()
        Assert.That(a + b, calculadora.Sumar(a, b))
    End Sub

End Class
