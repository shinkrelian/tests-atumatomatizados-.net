﻿Imports NUnit.Framework

<TestFixture()>
Public Class ActArrangeAssert

    <Test()>
    Public Sub ActArrangeAssertTest()

        'Arrange: Se preparan los objetos para el test
        Dim calculadora As Calculadora = New Calculadora()

        'Act: Ejecutar la acción sobre el SUT (Sistema bajo test)
        Dim resultado = calculadora.Sumar(2, 2)

        'Asser: Se validan que los resultados son los esperados
        Assert.AreEqual(4, resultado, "Sumar 2 y 2 no es 4")

    End Sub

End Class
