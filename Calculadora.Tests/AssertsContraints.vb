﻿Imports NUnit.Framework

<TestFixture>
Public Class AssertsConstraints

    <Test()>
    Public Sub AssertsConstraintsTest()

        Assert.That(True, [Is].True)
        Assert.That(False, [Is].False)
        Assert.That(2, [Is].EqualTo(2))
        Assert.That(2, [Is].Not.EqualTo(3))

        Assert.That(4, [Is].GreaterThan(3))
        Assert.That(4, [Is].GreaterThanOrEqualTo(3))
        Assert.That(3, [Is].LessThan(4))
        Assert.That(3, [Is].LessThanOrEqualTo(4))

        Assert.That(4, [Is].GreaterThan(3).And.LessThan(5))
        Assert.That(4, [Is].GreaterThan(3).or.LessThan(5))

        Dim anObject As Object
        Assert.That(anObject, [Is].Null)

        anObject = New Object
        Assert.That(anObject, [Is].Not.Null)

    End Sub

End Class
