﻿
Imports NUnit.Framework

<TestFixture(
    Author:="Javier Ortiz",
    Description:="Ejemplos de Uso de atributos en tests")>
Public Class Atributos

    <Repeat(3)>
    <Test(Description:="Test se repite 3 veces")>
    Public Sub RepiteTest()
        Assert.AreEqual(True, True)
    End Sub

    <Ignore("Falla")>
    <Retry(2)>
    <Test(Description:="Test se reintenta si falla, hasta 2 veces")>
    Public Sub ReintentaTest()
        Assert.AreEqual(False, True)
    End Sub

    <Order(1)>
    <Test(Description:="Order indica el orden relativo de ejecucción")>
    Public Sub Orden1()
        Assert.AreEqual(True, True)
    End Sub

    <Order(2)>
    <Test(Description:="Order indica el orden relativo de ejecucción")>
    Public Sub Orden2()
        Assert.AreEqual(True, True)
    End Sub

    <Category("Unitarios")>
    <Test(Description:="Categoría permite agrupar los tests")>
    Public Sub TestCategoriaUno()
        Assert.AreEqual(True, True)
    End Sub

    <Category("Integración")>
    <Test(Description:="Categoría permite agrupar los tests")>
    Public Sub TestCategoriaDos()
        Assert.AreEqual(True, True)
    End Sub

End Class
