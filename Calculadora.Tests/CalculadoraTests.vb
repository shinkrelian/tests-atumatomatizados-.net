﻿Imports Calculadora
Imports NUnit.Framework

<TestFixture()>
Public Class CalculadoraTests

    Protected Calculadora As Calculadora

    <SetUp()>
    Public Sub SetUp()
        Calculadora = New Calculadora()
    End Sub

    <Test()>
    Public Sub Sumar1Y1Es2()
        Dim resultado As Integer
        resultado = Calculadora.Sumar(1, 1)
        Assert.AreEqual(2, resultado, "Sumar 1 y 1 no es 2")
    End Sub

    <Test()>
    Public Sub Restar5Y2Es3()
        Dim resultado As Integer
        resultado = Calculadora.Restar(5, 2)
        Assert.AreEqual(3, resultado, "Restar 5 y 2 no es 3")
    End Sub

    <Test()>
    Public Sub Multiplicar4Y2Es8()
        Dim resultado As Integer
        resultado = Calculadora.Multiplicar(4, 2)
        Assert.AreEqual(8, resultado, "Multiplcar 4 y 2 no es 8")
    End Sub

    <Test()>
    Public Sub Dividir6Y2Es3()
        Dim resultado As Integer
        resultado = Calculadora.Dividir(6, 2)
        Assert.AreEqual(3, resultado, "Dividir 6 y 2 no es 3")
    End Sub

    <Test()>
    Public Sub Dividir7Y0LanzaUnaExcepcion()
        Assert.That(
            Sub()
                Calculadora.Dividir(7, 0)
            End Sub,
            Throws.[TypeOf](Of DivideByZeroException))
    End Sub

    <Test()>
    <Sequential()>
    Public Sub VariasSumas(
            <Values(1, 2, 3)> a As Integer,
            <Values(3, 5, 7)> b As Integer,
            <Values(4, 7, 10)> c As Integer)
        Dim resultado As Integer
        resultado = Calculadora.Sumar(a, b)
        Assert.AreEqual(c, resultado)
    End Sub

End Class
