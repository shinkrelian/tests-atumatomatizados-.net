﻿Imports System.IO
Imports NUnit.Framework

<TestFixture>
Public Class TestParametrizadosComplejos

    <Test, TestCaseSource(GetType(FileDataClass), "TestData")>
    Public Sub SumasParametrizadas(a As Integer, b As Integer, c As Integer)
        Dim calculadora As New Calculadora()
        Dim result = calculadora.Sumar(a, b)
        Assert.That(c, [Is].EqualTo(result))
    End Sub

    <Test()>
    Public Sub OpenFile()
        Dim di As New DirectoryInfo(TestContext.CurrentContext.TestDirectory)
        Dim fileName As String = di.Parent.Parent.FullName + "\Datos.txt"

        Dim objReader As New System.IO.StreamReader(fileName)

        Do While objReader.Peek() <> -1
            TestContext.WriteLine(objReader.ReadLine())
        Loop

    End Sub

End Class

Public Class FileDataClass
    Public Shared ReadOnly Iterator Property TestData() As IEnumerable
        Get
            Yield New TestCaseData(1, 3, 4)
            Yield New TestCaseData(3, 4, 7)
        End Get
    End Property
End Class
