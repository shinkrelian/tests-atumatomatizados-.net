﻿Imports NUnit.Framework

<TestFixture()>
Public Class CicloDeVida

    <OneTimeSetUp()>
    Public Sub OneTimeSetUp()
        TestContext.WriteLine("OneTimeSetUp: Se ejecuta una vez antes de todos los tests")
    End Sub

    <SetUp()>
    Public Sub SetUp()
        TestContext.WriteLine("SetUp: Se ejecuta antes de cada test")
    End Sub

    <Test()>
    Public Sub TestUno()
        TestContext.WriteLine("Test Uno")
    End Sub

    <Test()>
    Public Sub TestDos()
        TestContext.WriteLine("Test Dos")
    End Sub

    <TearDown()>
    Public Sub TearDown()
        TestContext.WriteLine("TearDown: Se ejecuta después de cada test")
    End Sub

    <OneTimeTearDown()>
    Public Sub OneTimeTearDown()
        TestContext.WriteLine("OneTimeTearDown: Se ejecuta una vez después todos los tests")
    End Sub

End Class