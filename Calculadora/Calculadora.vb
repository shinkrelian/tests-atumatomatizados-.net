﻿Imports Interfaces

Public Class CalculadoraFake
    Implements ICalculadora

    Public Function Sumar(a As Integer, b As Integer) As Integer Implements ICalculadora.Sumar
        Return 4
    End Function
End Class

Public Class Calculadora
    Implements ICalculadora

    Public Function Sumar(a As Integer, b As Integer) As Integer Implements ICalculadora.Sumar
        Return a + b
    End Function

    Public Function Restar(ByVal a As Integer, ByVal b As Integer) As Integer
        Return a - b
    End Function

    Public Function Multiplicar(ByVal a As Integer, ByVal b As Integer) As Integer
        Return a * b
    End Function

    Public Function Dividir(ByVal a As Integer, ByVal b As Integer) As Integer
        If b = 0 Then
            Throw New DivideByZeroException()
        End If
        Return a / b
    End Function

End Class

