﻿Public Interface IAccesoDatos
    Function ExisteDni(dni As String) As Boolean
End Interface


Public Class Servicio
    Private _accesoDatos As IAccesoDatos

    Public Sub New(a As IAccesoDatos)
        _accesoDatos = a
    End Sub


    Public Function CrearPersona(dni As String) As Boolean
        Dim accesoDatos As New AccesoDatos
        If _accesoDatos.ExisteDni(dni) Then
            Return False
        End If

        Return True
    End Function

End Class
