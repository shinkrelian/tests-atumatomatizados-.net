﻿Imports Interfaces
Imports Ninject
Imports Moq

Public Module Main

    Sub Main()
        Dim kernel As New Ninject.StandardKernel

        Dim mock As New Mock(Of ICalculadora)



        kernel.Bind(Of ICalculadora).To(Of Calculadora.CalculadoraFake)()

        Dim form = kernel.Get(Of CalcForm)
        Application.Run(form)
    End Sub

End Module
