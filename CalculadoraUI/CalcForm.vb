﻿Imports Interfaces
Imports Ninject

Public Class CalcForm
    Private calculadora As ICalculadora

    Public Sub New(calc As ICalculadora)
        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        calculadora = calc
    End Sub

    Public Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Label1.Text = calculadora.Sumar(
            Integer.Parse(TextBox1.Text),
            Integer.Parse(TextBox2.Text))
    End Sub

    Public Sub SetTexbox1(value As String)
        TextBox1.Text = value
    End Sub

    Public Sub SetTexbox2(value As String)
        TextBox2.Text = value
    End Sub

End Class
