﻿Public Class Game

    Private _rolls(21) As Integer
    Private _rollIndex As Integer = 1

    Public Sub Roll(pins As Integer)
        _rolls(_rollIndex) = pins
        _rollIndex += 1
    End Sub

    Public Function Score() As Integer
        Dim _score = 0
        Dim BolaInicialFrame = 1

        For frame As Integer = 1 To 10
            If (EsSpare(BolaInicialFrame)) Then
                _score += 10 + BonoSpare(BolaInicialFrame)
                BolaInicialFrame += 2
            ElseIf (EsChuza(BolaInicialFrame)) Then
                _score += 10 + BonoChuza(BolaInicialFrame)
                BolaInicialFrame += 1
            Else
                _score += PuntajeFrame(BolaInicialFrame)
                BolaInicialFrame += 2
            End If
        Next

        Return _score
    End Function

    Private Function EsChuza(bolaInicialFrame As Integer) As Boolean
        Return _rolls(bolaInicialFrame) = 10
    End Function

    Private Function BonoChuza(bolaInicialFrame As Integer) As Integer
        Return _rolls(bolaInicialFrame + 1) + _rolls(bolaInicialFrame + 2)
    End Function

    Private Function PuntajeFrame(i) As Integer
        Return _rolls(i) + _rolls(i + 1)
    End Function

    Private Function EsSpare(i As Integer) As Boolean
        Return _rolls(i) + _rolls(i + 1) = 10
    End Function

    Private Function BonoSpare(i) As Integer
        Return _rolls(i + 2)
    End Function

End Class
