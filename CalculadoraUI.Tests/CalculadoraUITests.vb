﻿
Imports Interfaces
Imports Moq
Imports NUnit.Framework

<TestFixture()>
Public Class CalculadoraUITests

    <Test>
    Public Sub PrimerMock()

        'Arrange
        'Crear los mocks requeridos
        Dim mock As New Mock(Of ICalculadora)
        Dim eventArgs = (New Mock(Of EventArgs)).Object
        Dim sender = (New Mock(Of Object)).Object
        Dim calcMock = mock.Object

        'Pasar el mock usand inyección de dependencias
        Dim calcForm = New CalcForm(calcMock)

        'Act
        calcForm.SetTexbox1("2")
        calcForm.SetTexbox2("3")
        calcForm.Button1_Click(sender, eventArgs)

        'Assert
        mock.Verify(Sub(c) c.Sumar(It.IsAny(Of Integer), It.IsAny(Of Integer)))
    End Sub

End Class
